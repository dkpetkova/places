#  Places

This is a project for testing purposes. It is a TabBar application which shows all bars around user location. There are two main sections - "List" which show list with bars, their names, theme image and distance to the user. "Map" section which shows the same information but with pins on the map, also annotation views with the bar name and distance. The application uses Google Places API.

##  REQUIREMENTS

### PREREQUISITES

+ Mac computer with Mac OS Sierra
+ Xcode - 9.0.1 or 9.1
+ Developer account in order to build the project on a real device
+ Clone the repository from https://dkpetkova@bitbucket.org/dkpetkova/places.git
+ Turn on your Location Service or simulate them with the simulator
+ Use if you want new GooglePlacesApiKey in ServiceRouter class
+ Use if you want bigger radius (500m) in fetchAllBarsAroundUserLocation method

### Main structure

+ Combined MVC and MVVM
+ View Models - models that are used for displaying information on the screen, it's a logic separated from the View Controller
+ Model - here is located the Core Data database manager who is reposnsible fore creating the model, also creating, fetching and updating objects
+ Utilities - LocationManager manages the location authorization from the device settings
+ Services - ServiceManager makes all requests and map the responses in objects with the new Swift 4 native way - Codable protocol
+ Extensions - Some protocol extensions which define additional funcionalities
+ Views - all custom views like cells, headers and etc
+ Main Interface - all storyboards and view controllers
+ Unit tests and UI tests for some general funcionalities

### BUILD SETTINGS

Base SDK: Latest iOS 11.0 and Swift 4

### RUNTIME SETTINGS

Deployment Target: iOS 11.0

> **WKWebView in PlaceTableViewCell can be created  from the code in order to work under iOS 11**

### DEBUG

Location funcionality and MKMapView are not fully working with simulator, you can use real device
Google Maps application is available only on a real device
I tested on iPad Air 2 with 11.0.3 and iPhone 6 with 11.0.3 and simulator iPhone 8 Plus

### ARCHIVE

+ Select Generic iOS Device for building.
+ Build the project using `Project -> Archive`.
+ Open `Window -> Organizer` (if not already open). Choose the last build and select `Upload to App Store…`. Follow the instructions.
+ Go to ***iTunes Connect*** and submit the build for distribution on App Store or for Beta Testing.

## CONTACTS

- Developer: Desislava Petkova

------

___© 2017 Desislava Petkova___
