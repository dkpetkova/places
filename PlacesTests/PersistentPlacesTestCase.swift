//
//  PersistentPlacesTestCase.swift
//  PlacesTests
//
//  Created by Desislava Petkova on 11/13/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import XCTest
import CoreData
import MapKit

@testable import Places

class PersistentPlacesTestCase: BaseTestCase, DataLoading {
    
    var mockDatabaseManager: DatabaseManager?
    
    override func setUp() {
        
        super.setUp()
        self.mockDatabaseManager = DatabaseManager.shared
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.flushData()
    }
    
    // MARK: - Tests
    
    func testExample() {
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testThatPersistentContainerIsNotNil() {
        
        XCTAssertNotNil(self.mockDatabaseManager?.inMemoryPersistentContainer)
    }
    
    func testThatPersistentContainerIsInMemory() {
        
        XCTAssertTrue(self.mockDatabaseManager?.inMemoryPersistentContainer.persistentStoreDescriptions.first?.type == NSInMemoryStoreType)
    }
    
    func testThatPlacesEntitiesAreTheRightNumber() {
        
        self.createPlacesFromJson { (error, places) in
            
            let context = self.mockDatabaseManager?.inMemoryPersistentContainer.viewContext
            
            let userLocation = CLLocation(latitude: CLLocationDegrees(51.51021369999999), longitude: CLLocationDegrees(-0.1315736))
            let radius = 500
            
            XCTAssertNotNil(context)
            XCTAssertNotNil(places?.places)
            
            if let context = context,
                let places = places?.places {
                
                for place in places {
                    
                    self.mockDatabaseManager?.createOrUpdatePlaceEntity(place: place, context: context)
                }
                
                do {
                    
                    try context.save()
                } catch {
                    
                    XCTAssertNotNil(error)
                }
                
                self.mockDatabaseManager?.fetchAllBarsAroundUserLocation(CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: radius, context: context) { (error, data) in
                 
                    XCTAssertTrue(data.count == 20)
                }
            }
        }
    }
    
    func testInsertingInDatabase() {

        let context = self.mockDatabaseManager?.inMemoryPersistentContainer.viewContext
        
        let place = Place(locationLatitude: 51.51, locationLongitude: -0.13, name: "Test Place", icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png", identifier: "123456")
        
        let userLocation = CLLocation(latitude: CLLocationDegrees(51.51021369999999), longitude: CLLocationDegrees(-0.1315736))
        let radius = 500
        
        XCTAssertNotNil(context)
        
        if let context = context {
            
            self.mockDatabaseManager?.createOrUpdatePlaceEntity(place: place, context: context)
            
            do {
                
                try context.save()
            } catch {
                
                XCTAssertNotNil(error)
            }
            
            self.mockDatabaseManager?.fetchAllBarsAroundUserLocation(CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: radius, context: context) { (error, data) in
                
                XCTAssertTrue(data.count == 1)
            }
        }
    }
    
    func testRemovingFromDatabase() {
        
        let context = self.mockDatabaseManager?.inMemoryPersistentContainer.viewContext
        
        let place = Place(locationLatitude: 51.51, locationLongitude: -0.13, name: "Test Place", icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png", identifier: "123456")
        
        let userLocation = CLLocation(latitude: CLLocationDegrees(51.51021369999999), longitude: CLLocationDegrees(-0.1315736))
        let radius = 500
        
        XCTAssertNotNil(context)
        
        if let context = context {
            
            self.mockDatabaseManager?.createOrUpdatePlaceEntity(place: place, context: context)
            
            do {
                
                try context.save()
            } catch {
                
                XCTAssertNotNil(error)
            }
            
            self.mockDatabaseManager?.fetchAllBarsAroundUserLocation(CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: radius, context: context) { (error, data) in
                
                context.delete(data.first!)
                do {
                    
                    try context.save()
                } catch {
                    
                    XCTAssertNotNil(error)
                }
                self.mockDatabaseManager?.fetchAllBarsAroundUserLocation(CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: radius, context: context) { (error, data) in
                    
                    XCTAssertTrue(data.count == 0)
                }
            }
        }
    }
    
    func testUpdatingInDatabase() {
        
        let context = self.mockDatabaseManager?.inMemoryPersistentContainer.viewContext
        
        let place = Place(locationLatitude: 51.51, locationLongitude: -0.13, name: "Test Place", icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png", identifier: "123456")
        
        let userLocation = CLLocation(latitude: CLLocationDegrees(51.51021369999999), longitude: CLLocationDegrees(-0.1315736))
        let radius = 500
        
        XCTAssertNotNil(context)
        
        if let context = context {
            
            // first time - inserting
            self.mockDatabaseManager?.createOrUpdatePlaceEntity(place: place, context: context)
            
            do {
                
                try context.save()
            } catch {
                
                XCTAssertNotNil(error)
            }
            
            // second time - updating
            self.mockDatabaseManager?.createOrUpdatePlaceEntity(place: place, context: context)
            
            do {
                
                try context.save()
            } catch {
                
                XCTAssertNotNil(error)
            }
            
            self.mockDatabaseManager?.fetchAllBarsAroundUserLocation(CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: radius, context: context) { (error, data) in
                
                XCTAssertTrue(data.count == 1)
            }
        }
    }
    
    // MARK: - Utilities
    
    func flushData() {
        
        let context = self.mockDatabaseManager?.inMemoryPersistentContainer.viewContext
        
        if let context = context {
            
            let fetchRequest: NSFetchRequest<PlaceEntity> = PlaceEntity.fetchRequest()
            do {
                
                let places = try context.fetch(fetchRequest)
                for place in places {
                    
                    context.delete(place)
                }
                do {
                    
                    try context.save()
                } catch {
                    
                }
            }
            catch {
            }
        }
    }
}
