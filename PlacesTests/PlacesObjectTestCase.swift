//
//  PlacesObjectTestCase.swift
//  PlacesTests
//
//  Created by Desislava Petkova on 11/13/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import XCTest
import CoreData
import MapKit

@testable import Places

class BaseTestCase: XCTestCase {
    
    override func setUp() {
        
        super.setUp()
    }
}

class PlacesObjectTestCase: BaseTestCase, DataLoading {
    
    override func setUp() {
        
        super.setUp()
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testExample() {
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testThatPlacesObjectIsNotNil() {
        
        self.createPlacesFromJson { (error, places) in
            
            XCTAssertNotNil(places)
        }
    }
    
    func testThatPlacesAreTheRightNumber() {
        
        self.createPlacesFromJson { (error, places) in
            
            XCTAssertTrue(places?.places?.count == 20)
        }
    }
    
    func testThatPlacesAreAroundTheUserLocation() {
        
        self.createPlacesFromJson { (error, places) in
            
            let userLocation = CLLocation(latitude: CLLocationDegrees(51.51021369999999), longitude: CLLocationDegrees(-0.1315736))
            let radius = 500
            let offset = 100
            
            XCTAssertNotNil(places?.places)
            
            if let places = places?.places {
                
                for place in places {
                    
                    let placeLocation = CLLocation(latitude: CLLocationDegrees(place.locationLatitude), longitude: CLLocationDegrees(place.locationLongitude))
                    let meters = userLocation.distance(from: placeLocation)
                    print(meters)
                    XCTAssertLessThan(meters, CLLocationDistance(radius+offset), "distance to the place is outside the radius")
                }
            }
        }
    }
}

protocol DataLoading {
    
}

extension DataLoading where Self: BaseTestCase {
    
    func createPlacesFromJson(completion: @escaping ((_ error: Error?, _ data: Places?) -> ())) {
        
        let placesJson = self.jsonFromResource("places", type: "json")
        
        do {
            
            let places = try JSONDecoder().decode(Places.self, from: placesJson!)
            completion(nil, places)
        } catch {
            
            completion(error, nil)
        }
    }
    
    func jsonFromResource(_ resource: String, type: String) -> Data? {
        
        let bundle = Bundle(for: BaseTestCase.self)
        let path = bundle.path(forResource: resource, ofType: type)
        let jsonURL = URL(fileURLWithPath: path!)
        
        do {
            
            let testJSONData = try Data(contentsOf: jsonURL)
            return testJSONData
        }
        catch {
            
            return nil
        }
    }
}
