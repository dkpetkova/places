//
//  TabBarUITestCase.swift
//  PlacesUITests
//
//  Created by Desislava Petkova on 11/13/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import XCTest

class TabBarUITestCase: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testTabBarListItemNotNil() {
        
        let tabBarsQuery = XCUIApplication().tabBars
        XCTAssertNotNil(tabBarsQuery.buttons["List"])
    }
    
    func testTabBarMapItemNotNil() {
        
        let tabBarsQuery = XCUIApplication().tabBars
        XCTAssertNotNil(tabBarsQuery.buttons["Map"])
    }
    
    func testTappingOnTabBarListItem() {
        
        let tabBar = XCUIApplication().tabBars
        tabBar.buttons["List"].tap()
        XCTAssertTrue(tabBar.buttons["List"].isSelected)
    }
    
    func testTappingOnTabBarMapItem() {
        
        let tabBar = XCUIApplication().tabBars
        tabBar.buttons["Map"].tap()
        XCTAssertTrue(tabBar.buttons["Map"].isSelected)
    }
    
    func testIfListScreenHasTable() {
        
        let tabBar = XCUIApplication().tabBars
        tabBar.buttons["List"].tap()
        let table = XCUIApplication().tables.firstMatch
        XCTAssertTrue(table.exists)
    }
    
    func testIfMapScreenHasMap() {
        
        let tabBar = XCUIApplication().tabBars
        tabBar.buttons["Map"].tap()
        let map = XCUIApplication().maps.firstMatch
        XCTAssertTrue(map.exists)
    }
}
