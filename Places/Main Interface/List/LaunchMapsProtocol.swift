//
//  LaunchMapsProtocol.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import MapKit

protocol LaunchMapsProtocol: class {
    
    func launchMapApplication(place: PlaceViewModel)
}

extension LaunchMapsProtocol where Self: UIViewController {
    
    func launchMapApplication(place: PlaceViewModel) {
        
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            
            let googleMapsAction = UIAlertAction(title: "Google Maps", style: .default, handler: { (action) in
                
                self.launchGoogleMaps(lat: place.latitude, lng: place.longitude)
            })
            
            let appleMapsAction = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) in
                
                self.launchAppleMaps(lat: place.latitude, lng: place.longitude, name: place.name)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            let actionSheet = UIAlertController(title: place.name, message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(googleMapsAction)
            actionSheet.addAction(appleMapsAction)
            actionSheet.addAction(cancelAction)
            
            // temporal fix of iPad action sheet presentation
            actionSheet.popoverPresentationController?.sourceView = self.view
            
            self.present(actionSheet, animated: true, completion: nil)
        } else {
            
            self.launchAppleMaps(lat: place.latitude, lng: place.longitude, name: place.name)
        }
    }
    
    func launchAppleMaps(lat: CGFloat, lng: CGFloat, name: String) {
        
        let regionDistance: CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(Double(lat), Double(lng))
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
    
    func launchGoogleMaps(lat: CGFloat, lng: CGFloat) {
        
        UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=&daddr=\(lat),\(lng)&directionsmode=walking")!, options: [:], completionHandler: nil)
    }
}
