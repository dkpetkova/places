//
//  ListViewController.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import MapKit

private let SearchRadius = 500

class ListViewController: BaseTableViewController, LaunchMapsProtocol {
    
    private(set) var dataSource: [PlaceViewModel] = []
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "List"
        
        NotificationCenter.default.addObserver(self, selector: #selector(ListViewController.userLocationIsUpdated(notification:)), name: Notification.Name(rawValue: LocationManagerDidUpdateLocationNotificationName), object: nil)
        self.prepareDataSource(false) {}
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        LocationManager.shared.stopUpdatingLocation()
    }
    
    override func refreshControlActionWithCompletionBlock(completionBlock: @escaping () -> ()) {
    
        self.prepareDataSource(true) {
            
            completionBlock()
        }
    }
    
    // MARK: - Prepare Datasource
    
    func prepareDataSource(_ sync: Bool, completionBlock: @escaping () -> ()) {
        
        LocationManager.shared.startUpdatingLocation()
        
        if let userLocation = LocationManager.shared.coreLocationManager.location {
            
            DatabaseManager.shared.fetchAllBarsAroundUserLocation(sync, CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: SearchRadius) { (error, data) in
                
                let viewModels = data.prepareViewModels().sorted(by: { (a, b) -> Bool in
                    
                    return a.distance <= b.distance
                })
                
                self.dataSource = viewModels
                self.tableView.reloadData()
                completionBlock()
            }
        }
        else {
            
            completionBlock()
        }
    }
    
    // MARK: - Actions
    
    @objc func userLocationIsUpdated(notification: Notification) {
    
        if self.dataSource.count == 0 {
        
            self.performRefresh()
        }
    }
}
