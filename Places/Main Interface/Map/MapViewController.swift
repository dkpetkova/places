//
//  MapViewController.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import MapKit

private let SearchRadius = 500

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: CustomMapView?
    
    private(set) var dataSource: [PlaceViewModel] = []
    var userLocation: MKUserLocation?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Map"
        
        self.mapView?.showsUserLocation = true
        
        LocationManager.shared.startUpdatingLocation()
        
        self.prepareDataSource(false)
    }
    
    deinit {
        
        LocationManager.shared.stopUpdatingLocation()
    }
    
    // MARK: - Prepare Datasource
    
    func prepareDataSource(_ sync: Bool) {
        
        LocationManager.shared.startUpdatingLocation()
        
        if let userLocation = LocationManager.shared.coreLocationManager.location {
            
            DatabaseManager.shared.fetchAllBarsAroundUserLocation(sync, CGFloat(userLocation.coordinate.latitude), longitude: CGFloat(userLocation.coordinate.longitude), radius: SearchRadius) { (error, data) in
                
                let viewModels = data.prepareViewModels().sorted(by: { (a, b) -> Bool in
                    
                    return a.distance <= b.distance
                })
                
                self.dataSource = viewModels
                
                if let annotations = self.mapView?.annotations {
                    
                    self.mapView?.removeAnnotations(annotations)
                }
                for place in self.dataSource {
                    
                    let annotation = MKPointAnnotation()
                    annotation.title = place.name
                    annotation.subtitle = place.distanceString
                    annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(place.latitude), longitude: CLLocationDegrees(place.longitude))
                    self.mapView?.addAnnotation(annotation)
                }
            }
        }
    }
}


