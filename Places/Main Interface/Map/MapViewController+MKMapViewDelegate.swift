//
//  MapViewController+MKMapViewDelegate.swift
//  Places
//
//  Created by Martin on 11/11/2017.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import MapKit

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        if self.userLocation == nil  {
            
            let center = userLocation.coordinate
            self.mapView?.showLocation(center: center)
        }
        self.userLocation = userLocation
        
        if self.dataSource.count == 0 {
            
            self.prepareDataSource(true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is MKUserLocation) {
            
            let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: String(annotation.hash))
            
            pinView.animatesDrop = true
            pinView.canShowCallout = true
            
            return pinView
        }
        else {
            
            return nil
        }
    }
}
