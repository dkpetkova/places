//
//  BaseTableViewController.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    //MARK: - View Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    // MARK: - Refreshing
    
    func performRefresh() {
        
        self.performRefresh(animated: true)
    }
    
    func performRefresh(animated: Bool) {
        
        let h = self.refreshControl?.frame.size.height ?? 0
        self.tableView.setContentOffset(CGPoint(x: 0, y: -h * 2), animated: animated)
        self.refreshControl?.sendActions(for: UIControlEvents.valueChanged)
    }
    
    @IBAction func refreshControlAction(sender: Any?) {
        
        self.beginRefresh()
        
        self.refreshControlActionWithCompletionBlock { [weak self] () -> () in
            
            self?.endRefresh()
        }
    }
    
    func refreshControlActionWithCompletionBlock(completionBlock: @escaping () -> ()) {
        
        DispatchQueue.main.async {
            
            completionBlock()
        }
    }
    
    func beginRefresh() {
        
        self.refreshControl?.beginRefreshing()
    }
    
    func endRefresh() {
        
        self.refreshControl?.endRefreshing()
    }
}
