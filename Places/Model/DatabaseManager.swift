//
//  DatabaseManager.swift
//  Places
//
//  Created by Martin on 10/11/2017.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import CoreData

protocol DatabaseManagerProtocol {
    
    func fetchAllBarsAroundUserLocation(_ sync: Bool, _ latitude: CGFloat, longitude: CGFloat, radius: Int, completion: @escaping ((_ error: Error?, _ data: [PlaceEntity]) -> ()))
}

class DatabaseManager {
 
    static let shared = DatabaseManager()
    
    init() {
        
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "Places")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {

                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var inMemoryPersistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Places")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        description.configuration = "Default"
        
        container.persistentStoreDescriptions = [description]
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            
            do {
                
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension DatabaseManager: DatabaseManagerProtocol {
 
    func fetchAllBarsAroundUserLocation(_ sync: Bool, _ latitude: CGFloat, longitude: CGFloat, radius: Int, completion: @escaping ((_ error: Error?, _ data: [PlaceEntity]) -> ())) {
        
        let context = self.persistentContainer.viewContext
        
        self.fetchAllBarsAroundUserLocation(latitude, longitude: longitude, radius: radius, context: context) { (error, data) in
            
            if sync == false && data.count != 0 {
                
                completion(error, data)
            }
            else if sync == true || (sync == false && data.count == 0) {
                
                ServiceManager.shared.fetchAllBarsAroundUserLocation(latitude, longitude: longitude, radius: radius, completion: { (error, placesResponseObject) in
                    
                    if let places = placesResponseObject?.places {
                        
                        let context = self.persistentContainer.viewContext
                        
                        for place in places {
                            
                            self.createOrUpdatePlaceEntity(place: place, context: context)
                        }
                        
                        do {
                            
                            try context.save()
                        } catch {
                            
                            let error = NSError(domain: "There is Core Data insert error!", code: 101, userInfo: nil)
                            completion(error, [])
                        }
                        
                        self.fetchAllBarsAroundUserLocation(latitude, longitude: longitude, radius: radius, context: context, completion: completion)
                    }
                    else {
                        
                        completion(error, [])
                    }
                })
            }
            else {
                
                completion(error, [])
            }
        }
    }
    
    func fetchAllBarsAroundUserLocation(_ latitude: CGFloat, longitude: CGFloat, radius: Int, context: NSManagedObjectContext, completion: @escaping ((_ error: Error?, _ data: [PlaceEntity]) -> ())) {
        
        let fetchRequest: NSFetchRequest<PlaceEntity> = PlaceEntity.fetchRequest()
        
        do {
            
            let places = try context.fetch(fetchRequest)
            completion(nil, places)
        }
        catch {
            
            let error = NSError(domain: "There is Core Data fetch error!", code: 102, userInfo: nil)
            completion(error, [])
        }
    }
    
    func createOrUpdatePlaceEntity(place: Place, context: NSManagedObjectContext) {
        
        let fetchRequest: NSFetchRequest<PlaceEntity> = PlaceEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "identifier==%@", place.identifier)
        do {
            
            let places = try context.fetch(fetchRequest)
            if let placeEntity = places.first {
                
                placeEntity.identifier = place.identifier
                placeEntity.name = place.name
                placeEntity.icon = place.icon
                placeEntity.latitude = Float(place.locationLatitude)
                placeEntity.longitude = Float(place.locationLongitude)
            }
            else {
                
                let entityDescription = NSEntityDescription.entity(forEntityName: "PlaceEntity", in: context)
                let placeEntity = PlaceEntity(entity: entityDescription!, insertInto: context)
                
                placeEntity.identifier = place.identifier
                placeEntity.name = place.name
                placeEntity.icon = place.icon
                placeEntity.latitude = Float(place.locationLatitude)
                placeEntity.longitude = Float(place.locationLongitude)
            }
        }
        catch {

        }
    }
}
