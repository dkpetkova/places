//
//  PlaceEntity+CoreDataProperties.swift
//  Places
//
//  Created by Martin on 10/11/2017.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//
//

import Foundation
import CoreData

extension PlaceEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlaceEntity> {
        return NSFetchRequest<PlaceEntity>(entityName: "PlaceEntity")
    }

    @NSManaged public var icon: String
    @NSManaged public var identifier: String
    @NSManaged public var latitude: Float
    @NSManaged public var longitude: Float
    @NSManaged public var name: String
    
}
