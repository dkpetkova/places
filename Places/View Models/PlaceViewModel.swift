//
//  PlaceViewModel.swift
//  Places
//
//  Created by Martin on 10/11/2017.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import CoreLocation

let MetersInKilometer: CGFloat = 1000.0

protocol PlaceViewModelProtocol {
    
    var latitude: CGFloat { get }
    var longitude: CGFloat { get }
    var name: String { get }
    var iconURL: URL { get }
    var distance: CGFloat { get }
    var distanceString: String { get }
}

struct PlaceViewModel: PlaceViewModelProtocol {
    
    var latitude: CGFloat
    var longitude: CGFloat
    var name: String
    var iconURL: URL
    var distance: CGFloat {
        
        var distanceInMeters: CGFloat = 0.0
        let placeLocation = CLLocation(latitude: CLLocationDegrees(self.latitude), longitude: CLLocationDegrees(self.longitude))
        
        if let userLocation = LocationManager.shared.coreLocationManager.location {
            
            distanceInMeters = CGFloat(userLocation.distance(from: placeLocation))
        }
        return distanceInMeters
    }
    
    var distanceString: String {
        
        var distanceToString: NSString = ""
        let distanceInMeters = self.distance
        
        if distanceInMeters < MetersInKilometer {
            
            distanceToString = NSString(format: "%.2fm", distanceInMeters)
        }
        else {
            
            distanceToString = NSString(format: "%.2fkm", distanceInMeters/MetersInKilometer)
        }
        return String(distanceToString)
    }
}
