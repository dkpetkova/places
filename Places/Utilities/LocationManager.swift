//
//  LocationManager.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import Foundation
import CoreLocation

let LocationManagerNoLocationPermissionHandlerNotificationName = "LocationManagerNoLocationPermissionHandlerNotificationName"
let LocationManagerDidUpdateLocationNotificationName = "LocationManagerDidUpdateLocationNotificationName"

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    private(set) lazy var coreLocationManager: CLLocationManager = {
        
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        return locationManager
    }()
    
    // MARK: - Error Handling
    
    func handleNoLocationPermission() {
        
        print("GPS not permitted")
        
        NotificationQueue.default.enqueue(Notification(name: Notification.Name(rawValue: LocationManagerNoLocationPermissionHandlerNotificationName), object: self), postingStyle: NotificationQueue.PostingStyle.whenIdle)
    }
    
    // MARK: - CLLocationManager Wrapper
    
    func startUpdatingLocation() {
        
        if !CLLocationManager.locationServicesEnabled() {
            
            self.handleNoLocationPermission()
            return
        }
        
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            
            self.coreLocationManager.requestWhenInUseAuthorization()
            return
            
        case .restricted, .denied:
            
            self.handleNoLocationPermission()
            return
            
        default:
            print("Not handled authorization status.")
        }
        
        self.coreLocationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        
        self.coreLocationManager.stopUpdatingLocation()
    }
}

extension LocationManager  {
    
    // MARK: - CLLocationManagerDelegate
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
            
        case .authorizedWhenInUse:
            
            self.startUpdatingLocation()
            return
            
        case .restricted, .denied:
            
            self.handleNoLocationPermission()
            return
            
        default:
            print("Unhandled CLAuthorizationStatus: \(status)")
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        NotificationQueue.default.enqueue(Notification(name: Notification.Name(rawValue: LocationManagerDidUpdateLocationNotificationName), object: self), postingStyle: NotificationQueue.PostingStyle.now)
    }
}

