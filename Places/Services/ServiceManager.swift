//
//  ServiceManager.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

protocol ServiceManagerProtocol {
    
    func fetchAllBarsAroundUserLocation(_ latitude: CGFloat, longitude: CGFloat, radius: Int, completion: @escaping ((_ error: Error?, _ data: Places?) -> ()))
}

class ServiceManager {
    
    static let shared = ServiceManager()
    
    init() {
        
    }
}

extension ServiceManager: ServiceManagerProtocol {
    
    func fetchAllBarsAroundUserLocation(_ latitude: CGFloat, longitude: CGFloat, radius: Int, completion: @escaping ((_ error: Error?, _ data: Places?) -> ())) {
        
        let url = ServiceRouter.fetchAllBarsAroundUserLocationURL(latitude, longitude: longitude, radius: radius)
        let request = URLRequest(url: url)
        ServiceRequest.performRequest(request, completion: completion)
    }
}
