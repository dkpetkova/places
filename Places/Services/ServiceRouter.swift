//
//  ServiceRouter.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

struct ServiceRouter {
    
    static let GooglePlacesApiKey: String = "AIzaSyA566zrkYoPgeEE_Nn4PBqCt5X3z92Oerc"
    static let GooglePlacesBaseURL: String = "https://maps.googleapis.com/maps/api/place"
    
    static func fetchAllBarsAroundUserLocationURL(_ latitude: CGFloat, longitude: CGFloat, radius: Int) -> URL {
        
        let url = "\(ServiceRouter.GooglePlacesBaseURL)/nearbysearch/json?location=\(latitude),\(longitude)&radius=\(radius)&types=bar&key=\(ServiceRouter.GooglePlacesApiKey)"
        return URL(string: url)! // GET
    }
}

