//
//  Places.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

struct Places: Codable {
    
    let places: [Place]?
}

extension Places {
    
    enum GlobalKeys: String, CodingKey {
        
        case results = "results"
    }

    init(from decoder: Decoder) throws {
        
        let globalContainer = try decoder.container(keyedBy: GlobalKeys.self) 
        let results: [Place] = try globalContainer.decode([Place].self, forKey: .results)
        
        self.init(places: results)
    }
}
