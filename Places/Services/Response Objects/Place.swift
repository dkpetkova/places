//
//  Place.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

struct Place: Codable {
    
    let locationLatitude: CGFloat
    let locationLongitude: CGFloat
    let name: String
    let icon: String
    let identifier: String
}

extension Place {
    
    enum GlobalKeys: String, CodingKey {
    
        case geometry = "geometry"
        case name = "name"
        case icon = "icon"
        case identifier = "place_id"
    }
    
    enum GeometryKeys: String, CodingKey {
        
        case location = "location"
    }
    
    enum LocationKeys: String, CodingKey {

        case lat = "lat"
        case lng = "lng"
    }

    init(from decoder: Decoder) throws {
        
        let globalContainer = try decoder.container(keyedBy: GlobalKeys.self)
        let geometryContainer = try globalContainer.nestedContainer(keyedBy: GeometryKeys.self, forKey: .geometry)
        let locationContainer = try geometryContainer.nestedContainer(keyedBy: LocationKeys.self, forKey: .location)
        let locationLatitude: CGFloat = try locationContainer.decode(CGFloat.self, forKey: .lat)
        let locationLongitude: CGFloat = try locationContainer.decode(CGFloat.self, forKey: .lng)
        
        let name = try globalContainer.decode(String.self, forKey: .name)
        let icon = try globalContainer.decode(String.self, forKey: .icon)
        let identifier = try globalContainer.decode(String.self, forKey: .identifier)
        
        self.init(locationLatitude: locationLatitude, locationLongitude: locationLongitude, name: name, icon: icon, identifier: identifier)
    }
}


