//
//  ServiceRequest.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import Foundation

class ServiceRequest {
    
    static func performRequest<ResultType>(_ request: URLRequest, completion: @escaping ((_ error: Error?, _ data: ResultType?) -> ())) where ResultType: Codable {
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            
            do {
                
                let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(string ?? "")
                
                let urlResponse = response as? HTTPURLResponse
                print("statusCode: \(String(describing: urlResponse?.statusCode))")
                
                guard let data = data else {
                    
                    let error = NSError(domain: "There is no response data!", code: 10, userInfo: nil)
                    DispatchQueue.main.async {
                        
                        completion(error, nil)
                    }
                    return
                }
                
                let object = try JSONDecoder().decode(ResultType.self, from: data)
                
                DispatchQueue.main.async {
                    
                    completion(error, object)
                }
            }
            catch {
                
                DispatchQueue.main.async {
                    
                    completion(error, nil)
                }
            }
        })
        task.resume()
    }
}
