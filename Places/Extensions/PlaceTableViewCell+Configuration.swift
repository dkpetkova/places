//
//  PlaceTableViewCell+Configuration.swift
//  Places
//
//  Created by Desislava Petkova on 11/13/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import Foundation

extension PlaceTableViewCell {
    
    func configure(_ place: PlaceViewModel)  {
        
        self.nameTextLabel?.text = place.name
        self.distanceTextLabel?.text = place.distanceString
        let imageRequest = URLRequest(url: place.iconURL)
        self.iconWebView?.load(imageRequest)
    }
}
