//
//  PlaceEntity+Collection.swift
//  Places
//
//  Created by Martin on 10/11/2017.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit

extension Collection where Element == PlaceEntity {
    
    func prepareViewModels() -> [PlaceViewModel] {
        
        var viewModels: [PlaceViewModel] = []
        for place in self {
            
            let viewModel = PlaceViewModel(latitude: CGFloat(place.latitude), longitude: CGFloat(place.longitude), name: place.name, iconURL: URL(string: place.icon)!)
            viewModels.append(viewModel)
        }
        return viewModels
    }
}
