//
//  PlaceTableViewCell.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import UIKit
import WebKit

class PlaceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameTextLabel: UILabel?
    @IBOutlet weak var distanceTextLabel: UILabel?
    
    // For deployment target < iOS 11 - create WKWebView from the code
    @IBOutlet weak var iconWebView: WKWebView?
}
