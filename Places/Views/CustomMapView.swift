//
//  CustomMapView.swift
//  Places
//
//  Created by Desislava Petkova on 11/10/17.
//  Copyright © 2017 Desislava Petkova. All rights reserved.
//

import Foundation
import MapKit

class CustomMapView: MKMapView {
    
    func showLocation(center: CLLocationCoordinate2D, delta: CLLocationDegrees = 0.01) {
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: delta, longitudeDelta: delta))
        
        self.setRegion(region, animated: true)
    }
    
    func showLocation(center: CLLocationCoordinate2D, radius: CLLocationDistance) {
        
        let region = MKCoordinateRegionMakeWithDistance(center, radius, radius)
        
        self.setRegion(region, animated: true)
    }
}
